#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "cgvScene3D.h"

// Constructor methods -----------------------------------

cgvScene3D::cgvScene3D() { axes = true; }

cgvScene3D::~cgvScene3D() {}


// Public methods ----------------------------------------

void draw_axes(void) {
  GLfloat red[]={1,0,0,1.0};
  GLfloat green[]={0,1,0,1.0};
  GLfloat blue[]={0,0,1,1.0};

	glBegin(GL_LINES);
    glMaterialfv(GL_FRONT,GL_EMISSION,red);
		glVertex3f(100,0,0);
		glVertex3f(-100,0,0);

    glMaterialfv(GL_FRONT,GL_EMISSION,green);
		glVertex3f(0,100,0);
		glVertex3f(0,-100,0);

    glMaterialfv(GL_FRONT,GL_EMISSION,blue);
		glVertex3f(0,0,100);
		glVertex3f(0,0,-100);
	glEnd();
}

void draw_shoebox(float x, float y, float z) {

	// shoebox bottom
	glPushMatrix();
	GLfloat color_piece[] = { 0,0.25,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, color_piece);
	glTranslatef(x, y, z);
	glScalef(1.0f, 1.0f, 2.0f);
	glutSolidCube(1);
	glPopMatrix();

	//shoebox top
	glPushMatrix();
	GLfloat color_piece2[] = { 0,0.25,0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, color_piece2);
	glTranslatef(x, y, z);
	glTranslatef(0.0f, 0.5f, 0.0f);
	glScalef(1.0f, 1.0f, 2.0f);
	glScalef(1.05f, 0.25f, 1.05f);
	glutSolidCube(1);
	glPopMatrix();
}

void cgvScene3D::render(int scene) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window and the z-buffer

	// lights
  GLfloat light0[]={10,8,9,1}; // point light source
  glLightfv(GL_LIGHT0,GL_POSITION,light0);
  glEnable(GL_LIGHT0);
	
	glPushMatrix(); // store the model matrices

	  // draw the axes
	  if (axes) draw_axes();

	  // Scene selection based on menu option (int scene)
	  if (scene == SceneA) renderSceneB();
		else if (scene == SceneB) renderSceneB();
	  // else if (scene == SceneB) renderSceneA();

	glPopMatrix (); // restore the modelview matrix 
  glutSwapBuffers(); // it is used, instead of glFlush(), to avoid flickering
}




void cgvScene3D::renderSceneA() {

	// TODO: Practice 2a. Part A.
	draw_shoebox(0.0f, 0.0f, 0.0f);
	draw_shoebox(0.0f, 1.0f, 0.0f);
	draw_shoebox(0.0f, 2.0f, 0.0f);
	
}



void cgvScene3D::renderSceneB() {
	// TODO: Practice 2a. Part B. 
	// TODO: Update the code to render the number of instances and piles indicated in Practice 2a. Part C. 
	
	int x_max = 3;
	int y_max = 4;
	int z_max = 2;
	int x = 0;
	int y = 0;
	int z = 0;

	for (int i=0; i < get_pile(); i++) {
		if (y == y_max) {
			y = 0;
			x += 1;
		}
		if (x == x_max) {
			x = 0;
			z += 1;
		}
		draw_shoebox(x * 1.2f, y * 1.2f, z * 2.5f);
		y += 1;
	}
	std::cout << x << " " << y << " " << z << std::endl;

	// glPushMatrix();
	// glPopMatrix();
       
}


