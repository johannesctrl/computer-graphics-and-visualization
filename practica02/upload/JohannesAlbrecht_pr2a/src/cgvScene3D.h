#ifndef __CGVSCENE3D
#define __CGVSCENE3D

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>

class cgvScene3D {
	
	public: 
		const int SceneA = 1;
		const int SceneB = 2;

		const char *SceneA_Name = "Scene A"; 
		const char *SceneB_Name = "Scene B"; 

	protected:
		// Attributes
		
		bool axes;

		// TODO: Part C. Define the variables to add new pile of instances
		// int pile[3][4][2];
		int pile = 0;

	public:
		// Default constructor and destructor
		cgvScene3D();
		~cgvScene3D();

		// Methodse
		// method with the OpenGL calls to render the scene
	  void render(int scene);

		bool get_axes() {return axes;};
		void set_axes(bool _axes){axes = _axes;};
		int get_pile() { return pile; };

		// TODO: Part C. Define the methods to modify the variables defined above to add new pile of instances
		int increase() {
			// cout << "The Value of 'i' is " << i << endl;
			std::cout << "increase" << std::endl;
			pile += 1;
			std::cout << pile << std::endl;
			return pile;
		};

		void decrease() {
			std::cout << "decrease" << std::endl;
			if (pile > 0) {
				pile -= 1;
				std::cout << pile << std::endl;
			}

		};




protected:
	void renderSceneA();
	void renderSceneB();
};

#endif
