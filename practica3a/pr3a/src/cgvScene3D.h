#pragma once

#if defined(__APPLE__) && defined(__MACH__)

#include <GLUT/glut.h>

#include <OpenGL/gl.h>

#include <OpenGL/glu.h>

#else

#include <GL/glut.h>

#endif

#include "cgvQuadMesh.h"

class cgvScene3D {
	protected:
		// Attributes
		double angleX, angleY, angleZ; 
		
		bool axes;

	public:
		// Public attributes
		// TODO: Section B. Define the mesh as a cgvCylinder 

		// Default constructor and destructor
		cgvScene3D();
		~cgvScene3D();

		// Static methods

		// Methods
		// method with the OpenGL calls to render the scene
    void render();

		bool get_axes() {return axes;};
		void set_axes(bool _axes){axes = _axes;};

		// TODO: Section A. Add methods to rotate the model.
		void rotateX(float angle) { angleX += angle; };
		void rotateY(float angle) { angleY += angle; };
		void rotateZ(float angle) { angleZ += angle; };
		
		double get_rotation_x() { return angleX; };
		double get_rotation_y() { return angleY; };
		double get_rotation_z() { return angleZ; };

};

