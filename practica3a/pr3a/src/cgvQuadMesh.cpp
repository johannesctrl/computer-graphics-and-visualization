

#if defined(__APPLE__) && defined(__MACH__)

#include <GLUT/glut.h>

#include <OpenGL/gl.h>

#include <OpenGL/glu.h>

#else

#include <GL/glut.h>

#endif

#include <cstdlib>
#include <stdio.h>
#include <math.h>


#include "cgvQuadMesh.h"

#define PI 3.14159

// Constructors 

cgvQuadMesh::cgvQuadMesh(long int _num_vertices, float *_vertices, long int _num_quads, unsigned int *_quads) {

	num_vertices = _num_vertices; 
	vertices = new float[num_vertices * 3]; 
	for (long int i=0;i<(num_vertices * 3);++i) {
		vertices[i] = _vertices[i]; 
	}

	normals = NULL; 

	num_quads = _num_quads; 
	quads = new unsigned int[num_quads * 4]; 
	for (long int i=0;i<(num_quads * 4);++i) {
		quads[i] = _quads[i]; 
	}
}


cgvQuadMesh::~cgvQuadMesh() {

	if (vertices != NULL)
		delete[] vertices; 
	if (normals != NULL)
		delete[] normals; 
	if (quads != NULL)
		delete[] quads; 
}


// Public methods 

void cgvQuadMesh::render(void) {
	glShadeModel(GL_FLAT);

	/* Section B: Render with a vertex array and in Section E add the array of normal vectors */


}


cgvCylinder::cgvCylinder(float r, float h, int div_u, int div_v):cgvQuadMesh() {

	// TODO. Section B. Build the quad mesh representing the cylinder. 

	// TODO. Section C. Add the normal vectors. 


}

