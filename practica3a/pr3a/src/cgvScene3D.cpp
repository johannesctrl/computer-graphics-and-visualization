#include <cstdlib>
#include <stdio.h>

#include "cgvScene3D.h"
#include "cgvQuadMesh.h"


static GLfloat black[]={0, 0, 0, 0};
// Constructor methods -----------------------------------

cgvScene3D::cgvScene3D () {
	angleX = angleY = angleZ = 0.0; 
	axes = true;
	// // TODO. Section B: insert here the code to call the cylinder constructor

}

cgvScene3D::~cgvScene3D() {}


// Public methods ----------------------------------------

void draw_axes(void) {
  GLfloat red[]={1,0,0,1.0};
  GLfloat green[]={0,1,0,1.0};
  GLfloat blue[]={0,0,1,1.0};

	glBegin(GL_LINES);
    glMaterialfv(GL_FRONT,GL_EMISSION,red);
		glVertex3f(1000,0,0);
		glVertex3f(-1000,0,0);

    glMaterialfv(GL_FRONT,GL_EMISSION,green);
		glVertex3f(0,1000,0);
		glVertex3f(0,-1000,0);

    glMaterialfv(GL_FRONT,GL_EMISSION,blue);
		glVertex3f(0,0,1000);
		glVertex3f(0,0,-1000);
	glEnd();

}


void cgvScene3D::render(void) {
	GLfloat mesh_color[]={0,0.25,0};

	// lights
	GLfloat light0[]={5,5,5,1}; // point light source
	glLightfv(GL_LIGHT0,GL_POSITION,light0); // this light is placed here and it remains still 
	glEnable(GL_LIGHT0);
  
  
	// create the model
	glPushMatrix(); // store the model matrices

		// draw the axes
		if (axes) draw_axes();

		//glLightfv(GL_LIGHT0,GL_POSITION,light0); // the light is placed here and it moves with the scene
		glMaterialfv(GL_FRONT,GL_EMISSION,mesh_color);

		// Glu cylinder
		GLUquadric *cyl = gluNewQuadric();
		glRotated(get_rotation_x(), 1.0f, 0.0f, 0.0f);
		glRotated(get_rotation_y(), 0.0f, 1.0f, 0.0f);
		glRotated(get_rotation_z(), 0.0f, 0.0f, 1.0f);
		gluCylinder(cyl, 1, 1, 1, 20, 5);
		gluDeleteQuadric(cyl);
		cyl = NULL;
          
		// Section B: next call should be substituted by the new method to render the mesh
	

	glPopMatrix (); // restore the modelview matrix 
  
}

