#pragma once

#if defined(__APPLE__) && defined(__MACH__)

#include <GLUT/glut.h>

#include <OpenGL/gl.h>

#include <OpenGL/glu.h>

#else

#include <GL/glut.h>

#endif

#include <cstdio>


class cgvQuadMesh {
	protected:
		// Attributes

		long int num_vertices; // number of vertices of the mesh of triangles
		float *vertices; // array with (num_vertices * 3) coordinates of the vertices
		float *normals; // array with (num_vertices * 3) coordinates of the normal vector to each vertex 

		long int num_quads; // number of quads in the mesh
		unsigned int *quads; // array with (num_quads * 4) indexes and vertices of each quad array 


	public:
		// Constructor and destructor
		cgvQuadMesh(long int _num_vertices, float *_vertices, long int _num_quads, unsigned int *_quads);
                                                      

		~cgvQuadMesh();

		// Methods
		// method with the OpenGL calls to render the mesh of triangles
                void render();

protected:
    cgvQuadMesh():num_vertices(0), vertices(0), normals(0), num_quads(0), quads(0) {}
};

class cgvCylinder: public cgvQuadMesh {


public: 
	// TODO: Section B: generate a cylinder centered in the origin with radius r, height h and 
	//       with div_u and div_v partitions in u and v, respectively

    cgvCylinder(float r, float h, int div_u, int div_v); 

    
};

