#include <cstdlib>
#include <stdio.h>
#include <iostream>

#include "cgvInterface.h"

extern cgvInterface interface; // the callbacks must be static and this object is required to access to the variables of the class                   // ellos a las variables de la clase

// Constructor and destructor methods -----------------------------------

cgvInterface::cgvInterface ():currentCam(0) {
  // TODO: Practice 2B.b: Define top, lateral and front cameras, respectively
  // Point1 = pv = point of view is the position where the camera is located
  // Point2 = rp = reference point defines the position where the camera is looking at
  // Point3 = up = 
  // free
  camera[0] = cgvCamera(cgvPoint3D(3.0, 2.0, 4.0), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));
  // top
  camera[1] = cgvCamera(cgvPoint3D(0.0001, 5.0, 0.0), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));
  // front
  camera[2] = cgvCamera(cgvPoint3D(5.0, 0.0, 0.0), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));
  // left
  camera[3] = cgvCamera(cgvPoint3D(0.0001, 0.0, 5.0), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));

  // Set parallel and perspective parameters for all cameras equally
  for (int i = 0; i <= 3; i++) {
    // fovy = field of view angle on the y-axes
    // aspect = Specifies the aspect ratio that determines the field of view in the x direction. The aspect ratio is the ratio of x (width) to y (height).
    // zNear = Specifies the distance from the viewer to the near clipping plane (always positive).
    // zFar = Specifies the distance from the viewer to the far clipping plane (always positive).
    camera[i].setParallelParameters(1 * 3, 1 * 3, 0.1, 200);
    camera[i].setPerspParameters(70, 1, 0.1, 200);
  }


        
}

cgvInterface::~cgvInterface () {}


// Public methods ----------------------------------------
void cgvInterface::configure_environment(int argc, char** argv, 
			                       int _width_window, int _height_window, 
			                       int _pos_X, int _pos_Y, const char* _title)
													 {
	// initialization of the interface variables																	
	width_window = _width_window;
	height_window = _height_window;

	// initialization of the display window
	glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(_width_window,_height_window);
  glutInitWindowPosition(_pos_X,_pos_Y);
	glutCreateWindow(_title);

	glEnable(GL_DEPTH_TEST); // enable the removal of hidden surfaces by using the z-buffer
  glClearColor(1.0,1.0,1.0,0.0); // define the background color of the window

	glEnable(GL_LIGHTING); // enable the lighting of the scene
  glEnable(GL_NORMALIZE); // normalize the normal vectors required by the lighting computation. 

}

void cgvInterface::init_rendering_loop() {
	glutMainLoop(); // initialize the visualization loop of OpenGL
}

void cgvInterface::set_glutKeyboardFunc(unsigned char key, int x, int y) {
  // std::cout << std::endl;
  std::cout << key << " was pressed: ";
  switch (key) {
    case 'p':
      if (interface.camera[interface.currentCam].isParallel()) {
        interface.camera[interface.currentCam].setCameraType(CGV_PERSPECTIVE);
      }
      else {
        interface.camera[interface.currentCam].setCameraType(CGV_PARALLEL);
      }
      break;
    case 'v': // change the current camera to show these views: panoramic, top, front and lateral view
      std::cout << "Please change the view pressing the buttons from 1 to 4" << std::endl;
 	  break;
    case '+': // zoom in
      interface.camera[interface.currentCam].zoom(-0.1);
      // std::cout << "currentZoom = " << interface.camera[interface.currentCam].getCurrentZoom() << std::endl;
		break;
    case '-': // zoom out
      interface.camera[interface.currentCam].zoom(+0.1);
      // std::cout << "currentZoom = " << interface.camera[interface.currentCam].getCurrentZoom() << std::endl;
	  break;
    case 'n': // increment the distance to the near plane
      interface.camera[interface.currentCam].near(+0.2);
	  break;
    case 'N': // decrement the distance to the near plane
      interface.camera[interface.currentCam].near(-0.2);
	  break;
    case '1': // change the viewport between 1 and 4 views.
      interface.setCurrentCamIndex(0);
 	  break;
    case '2': // change the viewport between 1 and 4 views.
      interface.setCurrentCamIndex(1);
      break;
    case '3': // change the viewport between 1 and 4 views.
      interface.setCurrentCamIndex(2);
      break;
    case '4': // change the viewport between 1 and 4 views.
      interface.setCurrentCamIndex(3);
      break;
    case '5': // change the viewport between 1 and 4 views.
      if (interface.getDividedScreen()) {
        interface.setDividedScreen(false);
      }
      else {
        interface.setDividedScreen(true);
      }
      std::cout << "dividedScreen = " << interface.getDividedScreen() << std::endl;
      break;
    case 'a': // enable/disable the visualization of the axes
			interface.scene.set_axes(!interface.scene.get_axes());
	  break;
    case 27: // Escape key to exit
      exit(1);
    break;
  }
	glutPostRedisplay(); // renew the content of the window
}

void cgvInterface::set_glutReshapeFunc(int w, int h) {
  // dimension of the viewport with a new width and a new height of the display window 


  // store the new values of the viewport and the display window. 
  interface.set_width_window(w);
  interface.set_height_window(h);

  // set up the viewport
  glViewport(0, 0, interface.get_width_window(), interface.get_height_window());
  
  // Set up the kind of projection to be used
  interface.camera[interface.currentCam].apply();

}

void cgvInterface::set_glutDisplayFunc() {
	
  if (interface.getDividedScreen()) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window and the z-buffer
    glViewport(0, 0, interface.get_width_window(), interface.get_height_window()); // set up the viewport
    interface.camera[interface.currentCam].apply(); // Set up the kind of projection to be used
    interface.scene.render(); // Render the scene
	  glutSwapBuffers(); // refresh the window; it is used instead of glFlush() to avoid flickering
  }
  else {
    int x = interface.get_width_window();
    int y = interface.get_height_window();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window and the z-buffer
      // big view
      glViewport(0, 0, interface.get_width_window()*2/3, interface.get_height_window()); // set up the viewport
      interface.camera[0].apply(); // Set up the kind of projection to be used
      interface.scene.render(); // Render the scene
      // view a
      glViewport(x*2/3, y*2/3, x * 1/3, y * 1/3); // set up the viewport
      interface.camera[1].apply(); // Set up the kind of projection to be used
      interface.scene.render(); // Render the scene
      // view b
      glViewport(x * 2 / 3, y * 1 / 3, x * 1 / 3, y * 1 / 3); // set up the viewport
      interface.camera[2].apply(); // Set up the kind of projection to be used
      interface.scene.render(); // Render the scene
      // view c
      glViewport(x * 2 / 3, 0, x * 1 / 3, y * 1 / 3); // set up the viewport
      interface.camera[3].apply(); // Set up the kind of projection to be used
      interface.scene.render(); // Render the scene
    glutSwapBuffers(); // refresh the window; it is used instead of glFlush() to avoid flickering
  }


	
}

void cgvInterface::init_callbacks() {
	glutKeyboardFunc(set_glutKeyboardFunc);
	glutReshapeFunc(set_glutReshapeFunc);
	glutDisplayFunc(set_glutDisplayFunc);
}


