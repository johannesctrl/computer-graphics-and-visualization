#if defined(__APPLE__) && defined(__MACH__)

#include <GLUT/glut.h>

#include <OpenGL/gl.h>

#include <OpenGL/glu.h>

#else

#include <GL/glut.h>

#endif

#include <math.h>
#include <stdio.h>
#include <iostream>

#include "cgvCamera.h"
#include "cgvInterface.h"
extern cgvInterface interface; // the callbacks must be static and this object is required to access to the variables of the class                   // ellos a las variables de la clase

// Constructors
cgvCamera::cgvCamera(cgvPoint3D _PV, cgvPoint3D _rp, cgvPoint3D _up):camType(CGV_PARALLEL), currentZoom(1), currentNear(1)
{
	setCameraParameters(_PV, _rp, _up);
	setParallelParameters(1, 1, 1, 20);
	setPerspParameters(1, 1, 1, 20);
}


cgvCamera::~cgvCamera () {}

void cgvCamera::setCameraParameters(cgvPoint3D _PV, cgvPoint3D _rp, cgvPoint3D _up) {
	PV = _PV;
	rp = _rp;
	up = _up;
}

void cgvCamera::getCameraParameters(cgvPoint3D& _PV, cgvPoint3D& _rp, cgvPoint3D& _up) {
	_PV = PV;
	_rp = rp;
	_up = up;
}

void cgvCamera::setParallelParameters(double _xwhalfdistance, double _ywhalfdistance,
	double _znear, double _zfar)
{
	camType = CGV_PARALLEL; 

	xwmin = -_xwhalfdistance;
	xwmax = _xwhalfdistance;
	ywmin = -_ywhalfdistance;
	ywmax = _ywhalfdistance;
	znear = _znear;
	zfar = _zfar;

}


void cgvCamera::getParallelParameters(double& _xwmin, double& _xwmax, double& _ywmin, double& _ywmax, 
										double& _znear, double& _zfar)
{
	_xwmin = xwmin;
	_xwmax = xwmax;
	_ywmin = ywmin;
	_ywmax = ywmax;
	_znear = znear;
	_zfar = zfar;
}

void cgvCamera::setPerspParameters(double _fovy, double _aspect, double _znear, double _zfar) {

	camType = CGV_PERSPECTIVE;

	fovy = _fovy;
	aspect = _aspect;
	znear = _znear;
	zfar = _zfar;

}

void cgvCamera::getPerspParameters(double& _fovy, double& _aspect, double& _znear, double& _zfar) {

	_fovy = fovy;
	_aspect = aspect;
	_znear = znear;
	_zfar = zfar;

}

void cgvCamera::setCameraType(cameraType cT) {
	camType = cT;
	if (camType == 0) {
		std::cout << "parallel" << std::endl;
	}
	else {
		std::cout << "perspective" << std::endl;
	}
}

void cgvCamera::apply() {

// TODO: Practice 2b.C: Modify this method in order to adequately apply a zoom to the camera. 

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();

	if (camType==CGV_PARALLEL) {
		glOrtho(xwmin* currentZoom, xwmax * currentZoom, ywmin * currentZoom, ywmax * currentZoom, znear, zfar);
	}
	if (camType==CGV_PERSPECTIVE) {
		gluPerspective(fovy * currentZoom,aspect,znear,zfar);
		// gluPerspective(fovy * currentZoom, aspect, znear, zfar);
	}
	
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();
	// gluLookAt(PV[X],PV[Y],PV[Z], rp[X],rp[Y],rp[Z], up[X],up[Y],up[Z]);
	gluLookAt(PV[X] * currentNear,PV[Y] * currentNear,PV[Z] * currentNear, rp[X],rp[Y],rp[Z], up[X],up[Y],up[Z]);
}

void cgvCamera::zoom(double factor) {
	// TODO: Practice 2b.C: Implement these functions. 
	currentZoom = currentZoom + factor;
	std::cout << "currentZoom = " << currentZoom << std::endl;
}

void cgvCamera::near(double factor) {
	currentNear = currentNear + factor;
	std::cout << "currentNear = " << currentNear << std::endl;
}



